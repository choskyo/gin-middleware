module gitlab.com/choskyo/gin-middleware

go 1.16

require (
	github.com/gin-gonic/gin v1.7.3
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/rs/zerolog v1.23.0
)
