package middleware

import (
	"errors"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"github.com/rs/zerolog"
)

type CreateJwtOptions struct {
	Key    []byte
	Logger *zerolog.Logger
}

func CreateJwtMiddleware(options CreateJwtOptions) gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")

		if authHeader == "" {
			c.AbortWithStatus(401)
			return
		}

		headerParts := strings.Split(authHeader, " ")
		if len(headerParts) < 2 {
			c.AbortWithStatus(401)
			return
		}

		headerToken := headerParts[1]
		token, err := jwt.Parse(headerToken, func(t *jwt.Token) (interface{}, error) {
			if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
				return nil, errors.New("unexpected signing method")
			}

			return options.Key, nil
		})
		if err != nil {
			if options.Logger != nil {
				options.Logger.
					Err(err).
					Msg("Couldn't parse token")
			}

			c.AbortWithStatus(401)
			return
		}

		if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
			c.Set("uid", claims["sub"])
		} else {
			c.AbortWithStatus(401)
		}

		c.Next()
	}
}
